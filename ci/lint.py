#!/usr/bin/env python3

import sys
import os
from pylint.lint import Run
import anybadge

script = os.path.join(os.path.dirname(__file__), "../bin/uhepp")
cli = os.path.join(os.path.dirname(__file__), "cli")

try:
    os.remove(cli)
except FileNotFoundError:
    pass
os.link(script, cli)

args = [
    "--disable=C0302,R0914,R0912,R0915,C0209",
    os.path.join(os.path.dirname(__file__), "../setup.py"),
    "uhepp.__init__",
    cli,
]



results = Run(args, do_exit=False)
status = results.linter.msg_status
score = "%.2f" % results.linter.stats.global_note

thresholds = {8: 'red',
              9: 'orange',
              9.9: 'yellow',
              10: 'green'}
badge = anybadge.Badge('pylint', score, thresholds=thresholds)
badge.write_badge('pylint.svg', overwrite=True)

sys.exit(status)

#!/bin/bash

# Expects the following environment variables:
# GPG_KEY (as file), TWINE_PASSWORD, TWINE_USERNAME

cat "$GPG_KEY" | gpg --batch --import

pip install twine anybadge
twine upload --sign dist/*

pip install .
anybadge -o --color "#006dad" -l PyPI -v $(python -c "import uhepp; print(uhepp.__version__)") -f pypi.svg

Interfaces and Integrations
============================

Uhepp has been integrated into multiple frameworks or packages. This page
maintains a list of third-party software that interfaces with uhepp.

Common Analysis Framework
-------------------------

The `Common Analysis Framework <https://atlas-caf.web.cern.ch/>`_, or CAF,  is an event
processing framework used within the ATLAS collaboration to study data produced
at the LHC. CAF provides a custom uhepp-compatible plotter that generates JSON
files instead of graphics formats and optionally uploads the plot to
uhepp.org. To use the plotter, add the following lines to your visualize
configuration.

.. code-block:: yaml

  plotter: uhepp
  plotFormats: json
  # Collection id, e.g. 1
  plotter.uhepp.collection: 1


FreeForestML
------------

The `FreeForestML
<https://gitlab.sauerburger.com/cern/fsauerbu/freeforestml/>`_ framework (formally nnfwtbn),
is a python-based event processing
framework with a focus on neural network training. The default plotting
environment in FreeForestML is based on uhepp and exposes the internal objects with
the :code:`return_uhepp=True` flag. The following example is a modified version
of `this example <https://freeforestml.readthedocs.io/en/latest/Histogram.html>`_ to show
how to push a histogram to a collection.

.. code-block:: python

    plot = hist(df, v_higgs_m, 20, [s_bkg, s_data], range=(0, 200), selection=None,
                weight="weight",  ratio_label="Data / SM", return_uhepp=True)
    # plot is a uhepp histogram
    plot.show() 
    plot.push(1)

The FreeForestML also has a `dedicated example Notebook
<https://freeforestml.readthedocs.io/en/latest/UHepp.html>`_ to illustrate the
uhepp integration.


Coffea
------

The helper method :code:`uhepp.from_coffea()` takes a `Coffea
<https://coffeateam.github.io/coffea/index.html>`_ histogram as input and
converts it into uhepp histogram objects.

.. code-block:: python


      import numpy as np
      import coffea.hist as hist
      import uhepp

      # https://coffeateam.github.io/coffea/notebooks/histograms.html
      chist = hist.Hist("Counts",
                        hist.Cat("sample", "sample name"),
                        hist.Bin("x", "x value", 20, -10, 10),
                        hist.Bin("y", "y value", 20, -10, 10),
                        hist.Bin("z", "z value", 20, -10, 10),
      )

      xyz = np.random.multivariate_normal(mean=[1, 3, 7], cov=np.eye(3), size=10000)
      chist.fill(sample="sample 1", x=xyz[:,0], y=xyz[:,1], z=xyz[:,2])

      (hx, hy, hz), samples = uhepp.from_coffea(chist)

Latex
-----

Using plots stored on uhepp hub in a latex document involves the following
steps: Downloading the plot, rendering the plot, adding the
:code:`includegraphics` command to the latex document.
With the following snippet, you can automate this procedure and add plots directly
using the plot's uuid.


Add this snippet in the preamble of your document:

.. code-block:: latex

		\newcommand{\includeuhepp}[2]{
		  \immediate\write18{mkdir -p .uhepp_cache}
		  \immediate\write18{test -f .uhepp_cache/#2.json || uhepp pull #2 .uhepp_cache/#2.json}
		  \immediate\write18{test -f .uhepp_cache/#2.pdf  || uhepp render .uhepp_cache/#2.json .uhepp_cache/#2.pdf}
		  \includegraphics[width=#1\textwidth]{.uhepp_cache/#2.pdf}
		}

Embed plots with:

.. code-block:: latex

    \includeuhepp{1.0}{a378d2b0-cde2-4266-be9b-85945d94880d}


TRExFitter
----------

The ATLAS software `TRExFitter <https://trexfitter-docs.web.cern.ch/trexfitter-docs/>`_ 
provides the configuration option `UheppFormat` which,
if set to `TRUE`, will trigger the generation of pre- and post-fit plots in signal, 
control and validation regions in uhepp format. The output is a YAML file which can be
managed by the Python package `uhepp` or the command-line tool `uhepp`. A common
use pattern is to push generated files to `uhepp.org <https://uhepp.org/>`_.

Consult the `TRExFitter documentation
<https://trexfitter-docs.web.cern.ch/trexfitter-docs/interfacing_tools/uhepp/>`_ for further information. 


================
 Storage format
================

.. role:: bolditalic
  :class: bolditalic


The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to be
interpreted as described in :rfc:`2119`.

The *universal HEP plot* format is defined here in terms of JSON. However, the
actual textual representation of lists and maps is not specific to JSON. Plots
MAY be stored in other formats, e.g. YAML. Many tools are available to
conversion between popular formats like YAML and JSON.

Header
======

Every *universal HEP plot* MUST contain the following keys.

.. code-block:: json

  {
    "version": "0.4",
    "type": "type_of_histogram"
  }

Depending on the version of the format or the type of the plot, other keys might
be REQUIRED. Software reading universal HEP plots MUST check whether they
support the format's version and the plot type of a input file and raise an
exception

  - if the major version is larger than the support version, or
  - the type is not supported.

Minor version updates are backwards compatible. If the major version is
supported by *universal HEP plot* software, any unknown keyword MUST be ignored.

Type: Histogram
===============

As of version 0.4, `histogram` is the only valid `type`.

.. list-table:: Semantic format description
  :header-rows: 1

  *
    - Field
    - Type 
    - Required
    - Description

  * 
    - `metadata` 
    - `object` 
    - yes 
    - Information about the plot used to lookup, identify and retrieve it form a database. The metadata has no effect on the plotting. 

  * 
    - `metadata.date` 
    - `string` 
    -  yes 
    - ISO 8601 date string (e.g. `"2020-07-20T11:35:07.700Z"`) when the *universal HEP plot* was created  

  *
    - `metadata.filename` 
    - `string` 
    -  yes 
    - Default filename without file extension used when creating a plot  
    
  *
    - `metadata.Ecm_TeV` 
    - `float` 
    -  no 
    - Center-of-mass energy of the particle collisions in TeV, e.g `13.0` 

  *
    - `metadata.lumi_ifb` 
    - `float` 
    -  no 
    - Total integrated luminosity of the analyzed data in inverse fb, e.g. `139.0` 
    
  *
    - `metadata.author` 
    - `string` 
    - no 
    - Name, username or nickname of the person who created the *universal HEP plot* 

  *
    - `metadata.producer` 
    - `string` 
    - no 
    - Name of the analysis framework which created the *universal HEP plot* 
    
  *
    - `metdata.code_revision` 
    - `string` 
    - no 
    - Identifier of the analysis code from version control software, e.g. a git commit  
    
  *
    - `metadata.event_selection` 
    - `string` 
    - no 
    - An arbitrary string to give an indication of the event selection 
    
  *
    - `metadata.tags` 
    - `object` 
    - no 
    - Arbitrary key-value-pairs to simplify plot retrieval from a database. 
    
  *
    - `metadata.tags.KEY` 
    - `string` or `null` 
    - no 
    - Arbitrary value for the key `KEY`.  Using `null` can be useful to make set-like lookups `if 'KEY' in tags`. 
    

  *
    - `badge` 
    - `object` 
    - no 
    - Information about the branding badge, e.g. :bolditalic:`ATLAS` Internal 
    
  *
    - `badge.brand` 
    - `sting` or `null` 
    - if `badge` 
    - Name of the branding, e.g. :bolditalic:`ATLAS`
    
  *
    - `badge.label` 
    - `string` 
    - no 
    - String placed after the branding text, e.g. `"Internal"` 
    
  *
    - `badge.subtext` 
    - `string` 
    - no 
    - Text placed below branding text, e.g. :math:`\sqrt{s} = 13\,\mathrm{TeV}, 139\,\mathrm{fb}^{-1}` 

  *
    - `variable` 
    - `object` 
    - yes 
    - Specification of :math:`x`-axis 
    
  *
    - `variable.symbol` 
    - `string` 
    - yes 
    - Mathematical/physical symbol of the quantity on the :math:`x`-axis, e.g.
      :math:`m` 
    
  *
    - `variable.unit` 
    - `string` 
    - no 
    - Unit of the quantity on the :math:`x`-axis, e.g. GeV 
    
  *
    - `variable.unit_in_brackets`
    - `bool` 
    - no 
    - Display the unit in square brackets instead of as "var / unit". Default: false
    
  *
    - `variable.name` 
    - `string` 
    - no 
    - Plain-text name of the quantity on the :math:`x`-axis, e.g. "Mass" 
    
  *
    - `variable.log` 
    - `bool` 
    - no 
    - If true, plot :math:`x`-axis logarithmically, default: false 
    
  *
    - `bins` 
    - `object` 
    - yes 
    - Information about the binning the data and the plot 
    
  *
    - `bins.edges` 
    - `list` of `floats` 
    - yes 
    - Bin boundaries of the data in the `yields` section 
    
  *
    - `bins.rebin` 
    - `list` 
    - no 
    - Subset of bin edges used for plotting, allows rebinning at plot-time. All values must occur in `bins.edges`. If missing, the plot uses the native bin edges. 
    
  *
    - `bins.include_underflow` 
    - `bool` 
    - no 
    - If true, add the content of the underflow bin to the left-most bin. Default: false 
    
  *
    - `bins.include_overflow` 
    - `bool` 
    - no 
    - If true, add the content of the overflow bin to the right-most bin. Default: false 
    
  *
    - `bins.density_width` 
    - `bool` 
    - no 
    - If true, normalizes the yields to the given width. Default: false 
    
  *
    - `stacks` 
    - list 
    - yes 
    - Content of the main plot 
    
  *
    - `stacks[i]` 
    - object 
    - no 
    - A stack of multiple processes 
    
  *
    - `stacks[i].type` 
    - `"step"`, `"stepfilled"` or `"points"` 
    - yes 
    - Type of the stack. Type `"points"` doesn't is most suitable for single-process stacks. 
    
  *
    - `stacks[i].error` 
    - `"no"`, `"stat"`, `"syst"`, `"both"` 
    - no 
    - Method to compute error bar/band. Default: `"stat"` 
    
  *
    - `stacks[i].x_errorbar` 
    - `bool`
    - no 
    - Draw vertical error bar for points. Default: true

  *
    - `stack[i].keep_zero` 
    - `bool`
    - no 
    - Draw points whose value and uncertainty is zero. Default: false
    
  *
    - `stacks[i].content` 
    - `list` 
    - yes 
    - List of processes to stack 
    
  *
    - `stacks[i].content[j].yield` 
    - `list` of `string` 
    - yes 
    - List of internal names of a processes in the `yields` section. Yields of these processes are added. 
    
  *
    - `stacks[i].content[j].label` 
    - `string` or `null`
    - yes 
    - Human-readable label used in the legend of the plot. If `null`, don't show in legend
    
  *
    - `stacks[i].content[j].style` 
    - `object` 
    - no 
    - Key-value pairs to change style. See Style section 
    
  *
    - `y_axis` 
    - `object` 
    - no 
    - Specification of :math:`y`-axis 
    
  *
    - `y_axis.min` 
    - `float` 
    - no 
    - Lower limit of :math:`y`-axis 
    
  *
    - `y_axis.max` 
    - `float` 
    - no 
    - Upper limit of :math:`y`-axis 
    
  *
    - `y_axis.log` 
    - `bool` 
    - no 
    - If true, plot :math:`y`-axis logarithmically, default: false 
    
  *
    - `y_axis.label` 
    - `string` 
    - no 
    - Custom axis label 
    
  *
    - `y_axis.append_unit`
    - `bool`
    - no
    - Automatically append unit to custom label, default: false

  *
    - `v_lines` 
    - `list` 
    - no 
    - Additionaly vertical lines 
    
  *
    - `v_lines[i]` 
    - `object` 
    - no 
    - Specifical of a vertical line 
    
  *
    - `v_lines[i].x` 
    - `float` 
    - if `v_lines[i]` 
    - Position of vertical line in units of :math:`x`-axis 
    
  *
    - `v_lines[i].range` 
    - `[float, float]` 
    - no 
    - :math:`y`-axis range of verical line. By default, it streches the data range 
    
  *
    - `v_lines[i].style` 
    - `object` 
    - no 
    - Additional key-value pairs to change style. See Style section 
    
  *
    - `h_lines` 
    - `list` 
    - no 
    - Additionaly horizontal lines 
    
  *
    - `h_lines[i]` 
    - `object` 
    - no 
    - Specifical of a horizontal line 
    
  *
    - `h_lines[i].y` 
    - `float` 
    - if `h_lines[i]` 
    - Position of horizontal line in units of :math:`y`-axis 
    
  *
    - `h_lines[i].range` 
    - `[float, float]` 
    - no 
    - :math:`x`-axis range of horizontal line. By default, it spans the data range
    
  *
    - `h_lines[i].style` 
    - `object` 
    - no 
    - Additional key-value pairs to change style. See Style section 
    
  *
    - `ratio` 
    - `list` 
    - no 
    - Definition of the ratio plot. If missing, no ratio plot is included 
    
  *
    - `ratio[i]` 
    - `object` 
    - no 
    - Single component of the ratio plot 
    
  *
    - `ratio[i].type` 
    - `"step"`, `"stepfilled"` or `"points"`
    - if `ratio[i]` 
    - Type of this component of the ratio plot 

  *
    - `ratio[i].error` 
    - `"no"`, `"stat"`, `"syst"`, `"stat+syst"`, `"env"`, `"stat+env"`, `"stat+syst+env"`
    -  no 
    - Method to compute error bar/band. Default: `"stat"` 

  *
    - `ratio[i].x_errorbar` 
    - `bool`
    - no 
    - Draw vertical error bar for points. Default: true
    
  *
    - `ratio[i].keep_zero` 
    - `bool`
    - no 
    - Draw points whose numerator value and uncertainty is zero. Default: false
    
  *
    - `ratio[i].den_error`
    - `"no"`, `"stat"`, `"syst"`, `"stat+syst"`, `"env"`, `"stat+env"`, `"stat+syst+env"`
    -  no
    - Method to compute error bar/band of the *unity* line. Default: value of `error`

  *
    - `ratio[i].numerator` 
    - `list` of `string` 
    - if `ratio[i]` 
    - List of internal names of a processes in the `yields` section. Yields of these processes are added and used as numerator of the ratio plot. 
    
  *
    - `ratio[i].denominator` 
    - `list` of `string` 
    - no 
    - List of internal names of a processes in the `yields` section. Yields of these processes are added and used as denominator of the ratio plot. If missing, defaults to 1. 
    
  *
    - `ratio[i].style` 
    - `object` 
    - no 
    - Additional key-value pairs to change style. See Style section 
    
  *
    - `ratio_axis` 
    - `object` 
    - no 
    - Specification of :math:`y`-axis of ratio plot
    
  *
    - `ratio_axis.min` 
    - `float` 
    - no 
    - Lower limit of :math:`y`-axis of ratio plot
    
  *
    - `ratio_axis.max` 
    - `float` 
    - no 
    - Upper limit of :math:`y`-axis of ratio plot
    
  *
    - `ratio_axis.log` 
    - `bool` 
    - no 
    - If true, plot :math:`y`-axis of the ratio plot logarithmically, default: false 
    
  *
    - `ratio_axis.label` 
    - `string` 
    - yes 
    - :math:`y`-axis label of the ratio plot, should be of the form `"something / other"` 
    
  *
    - `ratio_axis.diff` 
    - `bool` 
    - no 
    - Draw the difference between *numerator* and *denominator* instead of the ratio, default: false 
    
  *
    - `yields` 
    - `object` 
    - yes 
    - Data storage, contains all yields used in the plots 

  *
    - `yields.NAME` 
    - `object` 
    - yes 
    - Data associated with the internal name `NAME`. The key `NAME` MUST NOT contain a slash. 
    
  *
    - `yields.NAME.base` 
    - `list` of `float` 
    - yes 
    - Nominal yield of process `NAME`. The first item corresponds to the underflow bin, the last item corresponds to the overflow bin. The number of items must match the definition of bin edges. 
    
  *
    - `yields.NAME.stat` 
    - `list` of `float` 
    - no 
    - Absolute statistical yield variation of process `NAME`. MUST be of same length as `base`. 
    
  *
    - `yields.NAME.syst` 
    - `list` of `float` 
    - no 
    - Precomputed, absolute systematic yield variation of process `NAME`. Must be of same length as `base`.  

  *
    - `yields.NAME.var_up` 
    - `object` 
    - no 
    - Collection of variations. The variations can be used to compute an uncertainty band with the `env` keyword. 
    
  *
    - `yields.NAME.var_up.VARNAME` 
    - `list` of `float` 
    - no 
    - Absolute yield for the given variation.  The key `VARNAME` MUST NOT contain a slash. MUST be of same length as `base`. Variations can be referred to directly via the internal name `NAME/VARNAME/up`. This makes plotting envelops easy.  
    
  *
    - `yields.NAME.var_down` 
    - `object` 
    - no 
    - Collection of variations. The variations can be used to compute an uncertainty band with the `env` keyword. 
    
  *
    - `yields.NAME.var_down.VARNAME` 
    - `list` of `float` 
    - no 
    - Absolute yield for the given variation.  The key `VARNAME` MUST NOT contain a slash. MUST be of same length as `base`. Variations can be referred to directly via the internal name `NAME/VARNAME/down`. This makes plotting envelops easy.  
    
  *
    - `layout.size` 
    - `list` of `float`, length 2 
    - no 
    - Width, height of the canvas in inch, default: [5, 5] 

  *
    - `layout.ratio_fraction` 
    - `float` 
    - no 
    - Fraction of the total height taken by the ratio panel, default: 0.25 
    
  *
    - `graphs` 
    - `list` 
    - no 
    - Additional arbitrary graphs of points of lines 
    
  *
    - `graphs[i]` 
    - `object` 
    - no 
    - Specification of a set of points or a line 
    
  *
    - `graphs[i].x` 
    - `list` of `float` 
    - yes 
    - The :math:`x` values of the graph 

  *
    - `graphs[i].y` 
    - `list` of `float` 
    - yes 
    - The :math:`y` values of the graph 
    
  *
    - `graphs[i].type` 
    - `"points"` or `"line"` 
    - no 
    - Flag whether the graph should be drawn as points of as a line, default: `"points"` 
    
  *
    - `graphs[i].x_errors` 
    - `list` of `float` 
    - no 
    - Error bars in :math:`x` direction. Option has no effect for `"line"` setting. 
    
  *
    - `graphs[i].y_errors` 
    - `list` of `float` 
    - no 
    - Error bars or error band in :math:`y` direction. 
    
  *
    - `graphs[i].label` 
    - `string` 
    - no 
    - Optional label used in the legend 
    
  *
    - `graphs[i].style` 
    - `object` 
    - no 
    - Custom styling of the points of the line 

Style
-----

Available style options are:
 - `linewidth`
 - `linestyle`
 - `color`
 - `edgecolor`
 - `marker`
 - `markersize`

Colors must be RGB(A) hex strings, 6 or 8 characters.

Example
-------

.. code-block::

  {
    "version": "0.4",
    "type": "histogram",
    "metadata": {
      "tags": {
         "category": "vbf",
         "region": "sr",
         "channel": "lephad"
      },
      "filename": "lephad_vbf1_sr_mmc",
      "event_selection": "LepHad VBF1 SR",
      "code": "218871de2d245e73f2717f866e2c12c6fb966d23",
      "lumi_ifb": 139,
      "Ecm_TeV": 13,
      "date": "2020-07-20T11:35:07.700Z",
      "author": "Frank Sauerburger",
      "producer": "nnfwtbn"
    },
    "badge": {
      "brand": "ATLAS",
      "label": "Internal",
      "subtext": "$\\sqrt{s} = 13\\,\\mathrm{TeV}, 139\\,\\mathrm{fb}^{-1}$"
    },
    "variable": {
      "symbol": "$m_{\\mathrm{mmc}}$",
      "unit": "GeV"
    },

    "bins": {
      "edges": [0, 50, 100, 150, 200],
      "include_underflow": false,
      "include_overflow": false
    },

    "normalize_stacks": false,
    "stacks": [
      {
        "type": "stepfilled",
        "error": "stat",
        "content": [
          {
            "label": "Background",
            "yield": ["ztt", "fake"]
          },
          {
            "label": "Signal",
            "yield": ["vbfh", "ggh"]
          }
        ]
      },
      {
        "type": "points",
        "content": [
          {
            "label": "Data",
            "yield": ["data"]
          }
        ]
      }
    ],

    "ratio": [
      {
        "type": "points",
        "error": "stat",
        "numerator": ["ztt", "fake"],
        "denominator": ["data"]
      }
    ],
    "ratio_axes": {
      "min": 0.4,
      "max": 1.6,
      "label": "Data / Bkg"
    },

    "yields": {
      "ggh": {
         "base":  [0, 0, 0, 4, 5, 0],
         "stat": [0, 0.2, 0.4, 0.1, 0.3, 0]
      },
      "vbfh": {
         "base":  [0, 0, 0, 2, 3, 0],
         "stat": [0, 0.2, 0.4, 0.1, 0.3, 0]
      },
      "ztt": {
         "base":  [0, 20, 30, 20, 10, 1],
         "stat": [0, 0.2, 0.4, 0.1, 0.3, 0]
      },
      "fake": {
         "base":  [0, 2, 3, 2, 1, 1],
         "stat": [0, 0.2, 0.4, 0.1, 0.3, 0]
      },
      "data": {
         "base":  [0, 20, 35, 25, 18, 10],
         "stat": [0, 0.2, 0.4, 0.1, 0.3, 0]
      }
    }
  }


If the Python package `uhepp` is installed, you can download and show the plot
with 

.. code-block:: python

  import uhepp
  plot = uhepp.pull("9821da74-8b16-4127-b5de-8b6caabd6800")
  plot.show()

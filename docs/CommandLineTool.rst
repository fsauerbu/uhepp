Command-line tool
=================

The command-line tool :program:`uhepp` automates simple routine tasks. The
invocation requires a subcommand. This document summarizes the available
subcommands. The tool ships with the default installation and should be placed
automatically into the user's :envvar:`PATH`.

All subcommands accept the two options `--api-url` and `--api-key`. The
`--api-url` overrides the API root-endpoint otherwise taken from the environment
variable :envvar:`UHEPP_API`. Please make sure that it includes a trailing slash.

The `--api-key` is the API token used for authentication. If the option is
missing, the token is taken from environment variable :envvar:`UHEPP_TOKEN`. If
both methods fail, an anonymous connection to the API is set up.

.. role:: bash(code)
   :language: bash

Command: pull
-------------

The :bash:`uhepp pull` subcommand downloads a plot from the central hub.

The required argument `UUID` identifies the plot on the remote server. The JSON
data from the server is saved in a local file. The filename is either given as a
second argument, or if missing, taken from the uhepp metadata with the added suffix
`.json`.

If the optional flag `--render` is added, the plot's :meth:`render` method is
called and the graphics file is written to disk. If the output filename is inferred
from the metadata, the suffix `.pdf` is appended instead of `.json`.

The command prints the new filename.

Command: push
-------------

The :bash:`uhepp push` subcommand uploads a plot from the central hub.

The required arguments are the `collection id` (not the URL) and a `filename`
containing the plot in JSON format.

The command prints the front end URL of the new plot.

.. _command-show:

Command: show
-------------

The :bash:`uhepp show` subcommand shows local or remote plots.

The subcommand takes only a single positional argument. It can either be a
`UUID` or the name of a local file. If the given string matches the format of a
`UUID`, it is used to download the plot from the central hub. Otherwise, the
given string is used as an input filename.

In both cases, the loaded plot is then shown in an interactive window.

Command: render
---------------

The :bash:`uhepp show` subcommand renders local or remote plots.

The first argument can be either a `UUID` or a `filename`, see
:ref:`command-show`. The second argument is the destination file. The plot is
rendered as a graphics file to the destination file.

Command: cpull
--------------

The :bash:`uhepp cpull` subcommand downloads all visible plots from a collection.

The first argument must be the `collection id`. The optional second argument
specified the `destination path` where downloaded plots are placed. The
destination directory is created if it does not exist. The file names are
inferred from the plot's metadata. By default, the plots are stored as JSON. The
filenames receive the suffix `.json`.

If the optional flag `--render` is given, the plots are converted to a graphics
file. The filenames receive the suffix `.pdf`.

The command prints the names of the created files.

.. warning::
  If the filenames inferred from the plot's metadata are not unique, the command
  will silently overwrite freshly downloaded plots with other freshly downloaded
  plots.

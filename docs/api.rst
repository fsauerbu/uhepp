Hub REST API
============

The documentation of the API is intended for developers who want to write custom
applications that interact with the hub. If you simply want to 
upload or download plots from https://uhepp.org/, use :func:`uhepp.pull` and
:meth:`uhepp.UHeppHist.push`, or the :doc:`command-line tool <CommandLineTool>`.

Endpoint summary
----------------

The following table summarizes commonly-used API endpoints. For the uhepp hub,
all paths need to be prefixed by the api-root, e.g., https://uhepp.org/api. Placeholders are
enclosed in `<` and `>`.

.. list-table:: Summary of API endpoints
  :header-rows: 1

  *
    - Path
    - Method 
    - Request Body
    - Response
    - Side effect

  * 
    - `/`
    - `GET`
    - 
    - List of possible endpoints
    - 

  *
    - `/plots`
    - `GET`
    - 
    - List of links to all visible plots
    - 

  *
    - 
    - `POST`
    - Collection URL, uhepp JSON
    - Uhepp plot in JSON format
    - Adds a new plot to the collection

  *
    - `/plots/<UUID>`
    - `GET`
    - 
    - Uhepp plot in JSON format
    - 

  *
    -
    - `DELETE`
    - 
    - 
    - Removes the plot from the hub

  *
    - `/collections`
    - `GET`
    - 
    - List of links to all visible collections
    - 

  *
    -
    - `POST`
    - Collection title, visibility level, description
    - The collection details
    - Adds a new collection to the hub

  *
    - `/collections/<ID>`
    - `GET`
    - 
    - The collection details
    - 

  *
    -
    - `PUT`
    - Collection title, visibility level, description
    - The updated collection details
    - Updates the details of a collection

  *
    -
    - `DELETE`
    - 
    - 
    - Removes the collection from the hub

  *
    - `/users`
    - `GET`
    - 
    - List of links to all visible users
    - 

  *
    - `/users/<username>`
    - `GET`
    - 
    - Profile information
    - 


Authentication and visibility
-----------------------------

.. note:: TODO

References
----------

Several endpoints use references to other API resources in the request or
response body. **References must be URLs.** 

For example, when a plot is added to a collection, the collection
references must be specified in the request body. Assuming the collection id is `1`,
the corresponding reference URL is `https://uhepp.org/api/collections/1`.

All endpoints return references to other resources as an URL. This makes it easy
for client code to navigate between resources.


Details 
-------

.. note:: TODO

Headers
-------

Most endpoints include additional headers. For resource instances, the `Link`
header with `rel="ui"` is added. The link points to the user interface that
represents the same resources.

Plot instances include the header `X-Resource-UUID` to indicate the
universally unique identifier of the resource. You can use the UUID as an URL
parameter.  This header is especially useful in the response of a plot upload.

For example, the response of the demo plot contains both headers:

.. code-block:: console

  $ curl -I https://uhepp.org/api/plots/a378d2b0-cde2-4266-be9b-85945d94880d/ 

  HTTP/1.1 200 OK
  Content-Type: application/json
  Link: <https://uhepp.org/p/a378d2b0-cde2-4266-be9b-85945d94880d>; rel="ui"
  X-Resource-UUID: a378d2b0-cde2-4266-be9b-85945d94880d
  [...]

  

Browsable API
-------------

A large part of the API is built with the `Django Rest Framework
<https://www.django-rest-framework.org/>`_, which provides a browsable front end
of the API itself.

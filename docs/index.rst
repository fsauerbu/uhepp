=====================
 Universal HEP plots
=====================


.. toctree::
   :maxdepth: 2
   :hidden:

   Introduction <self>
   why
   GettingStarted.ipynb
   interfaces
   CommandLineTool
   publications


.. toctree::
   :maxdepth: 2
   :caption: Advanced Topics
   :hidden:

   storage_format
   module_reference
   api


Universal HEP introduces an exchange format for high-energy physics plots
that couples the raw data with the representation into a single file. However,
the two parts are still separated, such that you can change its appearance and
composition.

.. image:: demo.png
  :width: 60%
  :alt: Example histogram showing random data with non-equidistent binning
  :align: center



What is Uhepp?
==============

The term Uhepp -- universal high-energy physics plot -- refers to three distinct
projects:

 - Uhepp is a **storage format** for high-energy physics plots, usually in JSON or
   YAML syntax. The format couples the raw data with its visual representation
   in a single file while keeping them separate. This allows changing the
   visual appearance, binning, content, labels, color scheme, etc. Since the two
   components (data and visual style) are coupled, it's easy to reproduce the same
   plot or a modified version months or years after the initial creation.
   Read more about :doc:`the semantic  of the file format <storage_format>`.

 - Uhepp is a **Python package**, installed with :code:`pip install uhepp`. The
   package provides useful tools to work with the storage format. It can,
   for example, can translate the semantic JSON into an object-oriented
   representation and vice versa. With
   the package, it's also easy to build new plots and store them in a JSON
   file. Besides interfacing with the storage format, the package
   provides methods to render a plot as a graphics format and interact with a
   central hub.

 - Uhepp might refer to the **central hub** at https://uhepp.org. The hub provides a
   central storage facility for uhepp plots exposed via a REST API. Plots can be
   uploaded, download, and shared via the API. The hub also exposes the plots via
   a web interface, which makes sharing and discussing plots with colleagues easy
   and convenient.

Quickstart
==========

You can easily install the package with :program:`pip`.

.. code-block:: console
  
  pip install uhepp


To dive right into the topic, download a publicly available histogram from
https://uhepp.org specified by a unique identifier.

>>> import uhepp
>>> hist = uhepp.pull("a378d2b0-cde2-4266-be9b-85945d94880d")

To view the download histogram, call the :meth:`uhepp.UHeppHist.show` method.

>>> hist.show()

Alternatively, you can change the binning of the histogram.

>>> hist.rebin_edges = [0, 40, 60, 80, 100, 120, 200]
>>> hist.show()

If you need to know the total *Data* yield, extract the :class:`uhepp.Yield` object
from the histogram.

>>> data_yield = hist.yields["data"]
>>> data_yield.sum()
608.0

The :doc:`Getting Started <GettingStarted>` guide walks you through the creation
of the above example histogram and how to save it locally and push it to
a remote hub.



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

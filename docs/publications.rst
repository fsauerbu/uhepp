Used in
=======

This list summarizes articles and publications using the uhepp package. The
list is incomplete.

 - ATLAS Collaboration, *Measurements of Higgs boson production cross-sections in
   the 𝐻 → 𝜏+𝜏− decay channel in 𝑝𝑝 collisions at √𝑠 = 13 TeV with the ATLAS
   detector*, `arXiv:2201.08269 <http://arxiv.org/abs/2201.08269>`_, submitted to JHEP
 - F. Sauerburger, ATLAS Collaboration, *Measurement of the Higgs boson coupling
   to 𝜏-leptons in proton-proton collisions at √𝑠 = 13 TeV with the ATLAS
   detector at the LHC*, `PoS EPS-HEP2021 (2022) 573 <https://pos.sissa.it/398/573>`_
 - ATLAS Collaboration, *Combined measurements of Higgs boson production and
   decay using up to 139 fb−1 of proton-proton collision data at √𝑠 = 13 TeV
   collected with the ATLAS experiment*, `ATLAS-CONF-2021-053 <https://cds.cern.ch/record/2789544>`_
